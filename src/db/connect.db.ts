import { createConnection } from 'typeorm';

/**
 * Connection with retry logic
 */
export const connectDb = async (retries = 5): Promise<void> => {
    while (retries) {
      try {
        await createConnection();
        break;
      } catch (err) {
        console.log(err);
        retries -= 1;
        console.log(`retries left: ${retries}`);
        // wait 5 seconds
        await new Promise(res => setTimeout(res, 5000));
      }
    }
  };