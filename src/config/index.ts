import convict from 'convict';

export const config = convict({
        log: {
            default: 'error',
            doc: 'The logging level.',
            format: ['error', 'warn', 'info', 'verbose', 'trace', 'debug', 'silly'],
            arg: 'LOG_LEVEL',
        }, 
        port: {
            default: 3000,
            doc: 'API port.',
            format: Number,
            arg: 'PORT',
        } 
  });

  config.validate({allowed: 'strict'});