import { Response, Request } from 'express';
import { logger } from './logger.util';

interface ErrorContent {
    request: Request,
    response: Response,
    message: string,
    status: number,
}

export const sendErrorResponse = (
    {
        request, response, message, status
    }: ErrorContent) => {
            logger.error({
                request: {
                    method: request.method,
                    url: request.originalUrl,
                    params: request.params,
                    query: request.query,
                    headers: request.headers,
                    body: request.body,
                    status: status,
                },
                error: {
                    message: message
                },
                time: new Date().toUTCString()
            })
            response.status(status || 500).send(message)
    }