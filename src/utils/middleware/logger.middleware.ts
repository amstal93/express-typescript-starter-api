// Use of 'finish' emit inspired by:
// https://stackoverflow.com/questions/47518962/specify-express-middleware-that-always-runs-at-the-end
import { Request, Response, NextFunction } from 'express';

import { logger } from '../logger.util';

/**
 * On event 'finish' of request, log the event.
 * @param request - Express request
 * @param response - Express response
 * @param next - Next point of chain
 */
export const loggerMiddleware = (request: Request, response: Response, next: NextFunction) => {
    logger.info({
        request: {
            url: request.originalUrl,
            headers: request.headers,
            method: request.method,
            body: request.body,
        },
        time: new Date().toUTCString()
    });

    return next();
};