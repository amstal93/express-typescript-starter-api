import winston from 'winston';

import {config} from '../config';

export const logger = winston.createLogger({
	level: config.get('log'),
	transports: [
		new winston.transports.Console()
	]
});