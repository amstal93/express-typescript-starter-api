import { GetRecipeById } from './get-recipes-by-id';
import { Recipe } from '../entity/recipe.entity';

jest.mock('../entity/recipe.entity');

describe('get-recipes-by-id', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    })
    it('exists', () => {
        expect(GetRecipeById).toBeDefined();
    })
    it('uses the recipe entity', async () => {
        const mockReq = 2;
        const mockResult = new Recipe();
        mockResult.name = "hi";
        const spyOn = jest.spyOn(Recipe, 'findOne').mockResolvedValue(mockResult)

        const result = await GetRecipeById(mockReq);

        expect(spyOn).toHaveBeenCalledTimes(1);
        expect(result).toEqual(mockResult);
    })
})