import { CreateRecipe } from './create-recipe';
import { Recipe } from '../entity/recipe.entity';

jest.mock('../entity/recipe.entity');

describe('create-recipe', () => {
    it('exists', () => {
        expect(CreateRecipe).toBeDefined();
    })
    it('uses the recipe entity', async () => {
        const mockReq = {name: "hi"};
        const mockResult = {
            identifiers: [ { id: 1 } ],
            generatedMaps: [ { id: 1 } ],
            raw: [ { id: 1 } ]
          }
        const spyOn = jest.spyOn(Recipe, 'insert').mockResolvedValue(mockResult)

        const result = await CreateRecipe(mockReq);

        expect(spyOn).toHaveBeenCalledTimes(1);
        expect(result).toEqual(mockResult);
    })
})