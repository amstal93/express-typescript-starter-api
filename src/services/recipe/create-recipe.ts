import type { InsertResult } from 'typeorm';
import { Recipe } from '../entity/recipe.entity';

export const CreateRecipe = async ({
    name 
}: {name: string}): Promise<InsertResult | Error> => {
    try {
        return await Recipe.insert({
            name
        });
    } catch (err) {
        return err;
    }
}