import { Recipe } from '../entity/recipe.entity';

export const GetAllRecipes = async (): Promise<Recipe[] | undefined | Error> => {
    try {
        return await Recipe.find();
    } catch (err) {
        return err;
    }
}