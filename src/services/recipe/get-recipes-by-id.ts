import { Recipe } from '../entity/recipe.entity';

export const GetRecipeById = async (id: number): Promise<Recipe | undefined | Error> => {
    try {
        return await Recipe.findOne({id});
    } catch (err) {
        return err;
    }
}