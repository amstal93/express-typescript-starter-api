import {Entity, PrimaryGeneratedColumn, Column, BaseEntity} from "typeorm";

@Entity()
export class Recipe extends BaseEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column('text', {nullable: false})
    name!: string;

}