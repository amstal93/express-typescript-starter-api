import Joi from "joi";

import { HttpError } from '../../../utils/http-error.util';
import {StatusCodes} from '../../../constants/status-codes';

export interface RecipeRequestDTO  {
    id: number;
}

// Validation on request input, throws a http error that is caught
// in the controller if doesn't match.
export const getRecipeByIdParamValidation = Joi.object({
    id: Joi.number().required().error( err => {
            throw new HttpError(err.toString(), StatusCodes.BAD_REQUEST);
    })
});

export const createRecipeBodyValidation = Joi.object({
    name: Joi.string().required().error( err => {
            throw new HttpError(err.toString(), StatusCodes.BAD_REQUEST);
    })
});